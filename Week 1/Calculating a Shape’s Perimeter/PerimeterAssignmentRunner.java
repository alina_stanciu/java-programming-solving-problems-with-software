import edu.duke.*;
import java.io.File;

public class PerimeterAssignmentRunner {
    public double getPerimeter (Shape s) {
        // Start with totalPerim = 0
        double totalPerimeter = 0.0;
        // Start wth prevPt = the last point 
        Point previousPoint = s.getLastPoint();
        // For each point currPt in the shape,
        for (Point currentPoint : s.getPoints()) {
            // Find distance from prevPt point to currPt 
            double currentDistance = previousPoint.distance(currentPoint);
            // Update totalPerim by currDist
            totalPerimeter = totalPerimeter + currentDistance;
            // Update prevPt to be currPt
            previousPoint = currentPoint;
        }
        // totalPerim is the answer
        return totalPerimeter;
    }

    public int getNumPoints (Shape s) {

        int numberOfPoints = 0;
        for(Point point : s.getPoints()) {
            numberOfPoints++;
        }
        return numberOfPoints;
    }

    public double getAverageLength(Shape s) {

        double length = getPerimeter(s);
        double numberOfSides = (double) getNumPoints(s);
        double averageLength = length / numberOfSides;
        return averageLength;
    }

    public double getLargestSide(Shape s) {

        Point lastPoint = s.getLastPoint();
        double largestSide = 0.0;
        
        for(Point point : s.getPoints()) {
            double distance = lastPoint.distance(point);
            
            if(distance > largestSide) {
                largestSide = distance;
            }
            lastPoint = point;
        }
        return largestSide;
    }

    public double getLargestX(Shape s) {

        Point lastPoint = s.getLastPoint();
        int lastPointX = lastPoint.getX();
        double largestX = (double) lastPointX;
        
        for(Point point : s.getPoints()) {
            int newX = point.getX();
            
            if(newX > largestX){
                largestX = (double) newX;
            }
        }
        return largestX;
    }

    public double getLargestPerimeterMultipleFiles() {

        DirectoryResource dr = new DirectoryResource();
        double largestPerimeter = 0.0;
        FileResource largestFile = null;
        
        for(File f : dr.selectedFiles()){
            FileResource file = new FileResource(f);
            Shape shape = new Shape(file);
            double perimeter = getPerimeter(shape);
            
            if(perimeter > largestPerimeter){
                largestPerimeter = perimeter;
            }
        }
        return largestPerimeter;
    }

    public String getFileWithLargestPerimeter() {

        DirectoryResource dr = new DirectoryResource();
        double largestPerimeter = 0.0;
        File largestFile = null;   
        
        for(File f : dr.selectedFiles()) {
            FileResource file = new FileResource(f);
            Shape shape = new Shape(file);
            double perimeter = getPerimeter(shape);
            
            if(perimeter > largestPerimeter){
                largestPerimeter = perimeter;
                largestFile = f;
            }
        }
        return largestFile.getName();
    }

    public void testPerimeter () {
        
        FileResource fr = new FileResource();
        Shape shape = new Shape(fr);
        double length = getPerimeter(shape);
        
        System.out.println("perimeter = " + length);
        System.out.println("Number of Points: " + getNumPoints(shape));
        System.out.println("Average length: " + getAverageLength(shape));
        System.out.println("Largest side: "  + getLargestSide(shape));
        System.out.println("Largest x:" + getLargestX(shape));
    }
    
    public void testPerimeterMultipleFiles() {
        
        double largest = getLargestPerimeterMultipleFiles();
        System.out.println("Largest perimeter file: " + largest);
    }

    public void testFileWithLargestPerimeter() {
        
        String file = getFileWithLargestPerimeter();
        System.out.println("Largest perimeter file is: " + file);
    }

    // This method creates a triangle that you can use to test your other methods
    public void triangle(){
        
        Shape triangle = new Shape();
        triangle.addPoint(new Point(0,0));
        triangle.addPoint(new Point(6,0));
        triangle.addPoint(new Point(3,6));
        for (Point p : triangle.getPoints()){
            System.out.println(p);
        }
        double perimeter = getPerimeter(triangle);
        System.out.println("perimeter = "+ perimeter);
    }

    // This method prints names of all files in a chosen folder that you can use to test your other methods
    public void printFileNames() {
        
        DirectoryResource dr = new DirectoryResource();
        
        for (File f : dr.selectedFiles()) {
            System.out.println(f);
        }
    }

    public static void main (String[] args) {
        
        PerimeterAssignmentRunner pr = new PerimeterAssignmentRunner();
        pr.testPerimeter();
    }
}