public class Part1 {
    
    public String findSimpleGene(String dna) {
        
        String result = "";
        int atg = dna.indexOf("ATG");
        int taa = dna.indexOf("TAA");
        
        if(atg == -1 && taa == -1){
            return "There are no ATG and TAA strands";
        }
        
        if(atg == -1) {
            return "There is no ATG strand";
        }
        
        
        if(taa == -1) {
            return "There is no TAA strand";
        }
        
        result = dna.substring(atg, taa + 3);
        
        if(result.length() % 3 == 0){
            return result;
        } else {
            return "Not a valid gene";
        }
        
    }
    
    public void testSimpleGene() {
    
      String gene = "";
      
      String dna = "AAAATACCAGTACCACTAAGGA";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna);
      System.out.println("The gene is: " + gene);
      
      dna = "ATCATGAACAACGGA";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna);
      System.out.println("The gene is: " + gene);
      
      dna = "ATCGAATCCAAT";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna);
      System.out.println("The gene is: " + gene);
      
      dna = "ATCATCATGGTGGTTTAAGAC";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna);
      System.out.println("The gene is: " + gene);
      
      dna = "ATGCGCCGTAA";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna);
      System.out.println("The gene is: " + gene);
      
      dna = "AAATGCCCTAACTAGATTAAGAAACC";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna);
      System.out.println("The gene is: " + gene);
    }
}