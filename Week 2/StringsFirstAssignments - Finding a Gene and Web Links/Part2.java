public class Part2 {

    public String findSimpleGene(String dna, String startCodon, String stopCodon) {
        
        String result = "";
        
        int ATG = dna.indexOf("ATG");
        int TAA = dna.indexOf("TAA", ATG + 3);
        
        int atg = dna.indexOf("atg");
        int taa = dna.indexOf("taa", atg + 3);
        
        boolean upperCase = false;
        
        if(dna.toUpperCase().equals(dna)) {
            upperCase = true;
        }
        
        if(upperCase) {
            if(ATG == -1 && TAA == -1) {
                return "There are no ATG and TAA strands";
            } else if(ATG == -1) {
                return "There is no ATG strand";
            } else if(TAA == -1) {
                return "There is no TAA strand";
            }
        } else {
            if(atg == -1 && taa == -1) {
                return "There are no atg and taa strands";
            } else if(atg == -1) {
                return "There is no atg strand";
            } else if(taa == -1) {
                return "There is no taa strand";
            }
        }
        
        if(upperCase){
            result = dna.substring(ATG, TAA + 3);
        } else {
            result = dna.substring(atg, taa + 3);
        }
        
        if(result.length() % 3 == 0){
            if(upperCase) {
                return result.toUpperCase();
            } else {
                return result.toLowerCase();
            }
        } else {
            return "Not a valid gene";
        }
        
    }
    
    public void testSimpleGene() {
    
      String gene = "";
      
      String startCodonUpperCase = "ATG";
      String stopCodonUpperCase = "TAA";
      
      String startCodonLowerCase = "atg";
      String stopCodonLowerCase = "taa";
      
      String dna = "AAAATACCAGTACCACTAAGGA";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna, startCodonUpperCase, stopCodonUpperCase);
      System.out.println("The gene is: " + gene);
      
      dna = "ATCATGAACAACGGA";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna, startCodonUpperCase, stopCodonUpperCase);
      System.out.println("The gene is: " + gene);
      
      dna = "ATCGAATCCAAT";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna, startCodonUpperCase, stopCodonUpperCase);
      System.out.println("The gene is: " + gene);
      
      //dna = "ATCATCATGGTGGTTTAAGAC";
      dna = "atcatcatggtggtttaagac";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna, startCodonLowerCase, stopCodonLowerCase);
      System.out.println("The gene is: " + gene);
      
      dna = "ATGCGCCGTAA";
      System.out.println("The DNA Strand is: " + dna);
      gene = findSimpleGene(dna, startCodonUpperCase, stopCodonUpperCase);
      System.out.println("The gene is: " + gene);
    }
}