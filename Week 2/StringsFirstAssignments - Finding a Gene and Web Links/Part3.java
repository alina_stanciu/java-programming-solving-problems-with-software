public class Part3 {

    public boolean twoOccurences(String stringA, String stringB) {
    
            int firstOccurance = stringB.indexOf(stringA);
            int secondOccurance = stringB.indexOf(stringA, firstOccurance + stringA.length());
            
            if(secondOccurance == -1){
                return false;
            } else {
                return true;
            }
    }
    
    public void testingTwoOccurences() {
    
    System.out.println("The first string: \"A story by Abby Long\" and the second string: \"by\"");
    System.out.println(twoOccurences("by", "A story by Abby Long"));
    
    System.out.println("The first string: \"banana\" and the second string: \"a\"");
    System.out.println(twoOccurences("a", "banana"));
    
    System.out.println("The first string: \"ctgtatgta\" and the second string: \"atg\"");
    System.out.println(twoOccurences("atg", "ctgtatgta") );
    }
    
    public String lastPart(String stringA, String stringB) {
        
        int occurence = stringB.indexOf(stringA);
        String finalPart = stringB.substring(occurence + stringA.length());
        
        if (occurence == -1) {
            return stringB;
        } else {
            return finalPart;
        }
    }
   
    public void testingLastPart() {
    
        String stringA = "an";
        String stringB = "banana";
        System.out.println("The part of the string after " + stringA + " in " + stringB +" is "+ lastPart(stringA, stringB));
        
        stringA = "rest";
        stringB = "deforestacion";
        System.out.println("The part of the string after " + stringA + " in " +stringB +" is "+ lastPart(stringA, stringB));
        
        stringA = "cio";
        stringB = "deforestacion";
        System.out.println("The part of the string after " + stringA + " in " +stringB +" is "+ lastPart(stringA, stringB));
        
        stringA = "zoo";
        stringB = "forest";
        System.out.println("The part of the string after " + stringA + " in " +stringB +" is "+ lastPart(stringA, stringB));
    }
}