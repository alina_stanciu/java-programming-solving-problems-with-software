public class Part1 {

   public int findStopCodon(String dna, int startIndex, String stopCodon) {
    
       int currentIndex = dna.indexOf(stopCodon, startIndex + 3);
        
       while(currentIndex != -1){
           int difference = currentIndex - startIndex;
            
           if (difference % 3 == 0) {
               return currentIndex;
           } else {
               currentIndex = dna.indexOf(stopCodon, currentIndex + 1);
           }
       }
       return -1;
   }
    
   public void testFindStopCodon() {
   
       String dna = "xxxyyyzzzTAAxxxyyyzzzTAAxxxyyyTAGzzzTGAzTAG";
       System.out.println("The DNA strand is: " + dna);
       
       int index = findStopCodon(dna,0,"TAA");
       
       if(index != 9) {
           System.out.println("Error at index 9, TAA not found");
       } else {
           System.out.println("The TAA stop codon is at index " + index);
       }
       
       index = findStopCodon(dna,9,"TAA");
       
       if(index != 21) {
           System.out.println("Error at index 21, TAA not found");
       } else {
           System.out.println("The TAA stop codon is at index " + index);
       }
       
       index = findStopCodon(dna,21,"TAA");
       
       if(index != 30) {
           System.out.println("Error at index 30, TAA not found");
       } else {
           System.out.println("The TAA stop codon is at index " + index);
       }
       
       index = findStopCodon(dna,21,"TAG");
       
       if(index != 30) {
           System.out.println("Error at index 30, TAG not found");
       } else {
           System.out.println("The TAG stop codon is at index " + index);
       }
       
       index = findStopCodon(dna,30,"TGA");
       
       if(index != 36) {
           System.out.println("Error at index 36, TGA not found");
       } else {
           System.out.println("The TGA stop codon is at index " + index);
       }
       
       index = findStopCodon(dna,36,"TAG");
       
       if(index != 40) {
           System.out.println("Error at index 40");
       } else {
           System.out.println("The TAG stop codon is at index " + index);
       }
   }
    
   public String findGene(String dna, int where) {
    
        int startIndex = dna.indexOf("ATG",where);
        
        if(startIndex == -1) {
            return "";
        }

        int taaIndex = findStopCodon(dna, startIndex, "TAA");
        int tagIndex = findStopCodon(dna, startIndex, "TAG");
        int tgaIndex = findStopCodon(dna, startIndex, "TGA");

        int minIndex = 0;

        if (taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
            minIndex = tgaIndex;
        } else {
            minIndex = taaIndex;
        }
        
        if (minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
            minIndex = tagIndex;
        }
        
        if (minIndex == -1) {
            return "";
        }
        return dna.substring(startIndex, minIndex + 3);
   }
    
   
   public void testFindGene() {
   
      String dna = "";  
      String gene = "";
      
      dna = "AATGCGTAATTAGTCG";
      System.out.println("The DNA strand is: " + dna);
      gene = findGene(dna, 0);
      System.out.println("The gene is: " + gene);
      
      dna = "CGATGGTTGATAAGCCTAAGCTATAA";
      System.out.println("The DNA strand is: " + dna);
      gene = findGene(dna, 0);
      System.out.println("The gene is: " + gene);
      
      dna = "CCGGATGTACCGGATATAGCGATG";
      System.out.println("The DNA strand is: " + dna);
      gene = findGene(dna, 0);
      System.out.println("The gene is: " + gene);
      
      dna = "ATGCCGGTTATTAA";
      System.out.println("The DNA strand is: " + dna);
      gene = findGene(dna, 0);
      System.out.println("The gene is: " + gene);
      
      dna = "ATGATGCGTATAG";
      System.out.println("The DNA strand is: " + dna);
      gene = findGene(dna, 0);
      System.out.println("The gene is: " + gene);
   }
   
   public void printAllGenes(String dna) {

        int startIndex =0;

        while(true) {
            String currentGene = findGene(dna, startIndex);

            if(currentGene.isEmpty()){
                break;
            }
            System.out.println(currentGene);

            startIndex = dna.indexOf(currentGene, startIndex) + currentGene.length();
        }
   }
   
   public void testPrintAllGenes() {
       
       String dna = "";
       
       dna = "ATGATCTAATTTATGCTGCAACGGTGAAGA";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The genes are: ");
       printAllGenes(dna);
       
       dna = "ATGATCATAAGAAGATAATAGAGGGCCATGTAA";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The genes are: ");
       printAllGenes(dna);
       
       dna = "CCATGCATATATAGCCATGGTTAA";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The genes are: ");
       printAllGenes(dna);
   }
}