public class Part2 {

    public int howMany(String stringA, String stringB) {
    
        int count = 0;
        
        int firstOccurance = stringB.indexOf(stringA);
        
        while(stringB.indexOf(stringA, firstOccurance) != -1 && firstOccurance != -1) {
            count++;
            firstOccurance = stringB.indexOf(stringA, firstOccurance + stringA.length());
        }
        
        return count;
    }
    
    public void testHowMany(){
        
        String stringA = "";
        String stringB = "";
       
        stringA = "GUT";
        stringB = "GGUUTTTUGGUTT";
        System.out.println("How many times does \"" + stringA + "\" appear in \"" + stringB + "\"?");   
        howMany(stringA,stringB); 
        if (howMany(stringA,stringB) == 0) {
            System.out.println("No occurrence has been found!");
        } else{
            System.out.println(howMany(stringA,stringB) + " time(s)");
        }
        
        stringA = "AA";
        stringB = "ARARAAAARARA";
        System.out.println("How many times does \"" + stringA + "\" appear in \"" + stringB + "\"?");   
        howMany(stringA,stringB);
        if (howMany(stringA,stringB) == 0) {
            System.out.println("No occurrence has been found!");
        }
        else{
            System.out.println(howMany(stringA,stringB) + " time(s)");
        }
        
        stringA = "XX";
        stringB = "XYYXZXZO";
        System.out.println("How many times does \"" + stringA + "\" appear in \"" + stringB + "\"?");   
        howMany(stringA,stringB);
        if (howMany(stringA,stringB) == 0) {
            System.out.println("No occurrence has been found!");
        }else{
            System.out.println(howMany(stringA,stringB) + " time(s)");
        }
        
        stringA = "ABRA";
        stringB = "ABRACABRAABRADABRADUUU";
        System.out.println("How many times does \"" + stringA + "\" appear in \"" + stringB + "\"?");   
        howMany(stringA,stringB);
        if (howMany(stringA,stringB) == 0) {
            System.out.println("No occurrence has been found!");
        } else{
            System.out.println(howMany(stringA,stringB) + " time(s)");
        }
    }
}