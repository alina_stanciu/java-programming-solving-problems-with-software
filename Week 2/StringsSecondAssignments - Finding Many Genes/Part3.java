public class Part3 {

   public int findStopCodon(String dna, int startIndex, String stopCodon) {
    
       int currentIndex = dna.indexOf(stopCodon, startIndex + 3);
        
       while(currentIndex != -1){
           int diff = currentIndex - startIndex;
            
           if (diff % 3 == 0) {
               return currentIndex;
           } else {
               currentIndex = dna.indexOf(stopCodon, currentIndex + 1);
           }
       }
       return -1;
   }
    
   public String findGene(String dna, int where) {
    
       int startIndex = dna.indexOf("ATG",where);
        
       if(startIndex == -1) {
           return "";
       }

       int taaIndex = findStopCodon(dna, startIndex, "TAA");
       int tagIndex = findStopCodon(dna, startIndex, "TAG");
       int tgaIndex = findStopCodon(dna, startIndex, "TGA");

       int minIndex = 0;

       if (taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
           minIndex = tgaIndex;
       } else {
           minIndex = taaIndex;
       }
        
       if (minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
           minIndex = tagIndex;
       }
        
       if (minIndex == -1) {
           return "";
       }
       return dna.substring(startIndex, minIndex + 3);
   }
   
   public void printAllGenes(String dna) {

        int startIndex =0;

        while(true) {
            String currentGene = findGene(dna, startIndex);

            if(currentGene.isEmpty()){
                break;
            }
            System.out.println(currentGene);

            startIndex = dna.indexOf(currentGene, startIndex) + currentGene.length();
        }
   }
   
   public int countGenes(String dna) {
    
       int startIndex =0;
       int count = 0;

       while(true) {
           String currentGene = findGene(dna, startIndex);

           if(currentGene.isEmpty()){
               break;
           }
           count++;
           startIndex = dna.indexOf(currentGene, startIndex) + currentGene.length();
       }
       return count;
   }
   
   public void testCountGenes() {
    
       String dna = "";
       
       dna = "ATGATCATAAGAAGATAATAGAGGGCCATGTAA";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("There are " + countGenes(dna) + " gene(s) in the give DNA strand");
       
       dna = "CCATGCGGCTTAGG";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("There are " + countGenes(dna) + " gene(s) in the give DNA strand");
       
       dna = "ATGATTACCGCGTAACCATGTGAGCCGATGAATTAGATGCTAG";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("There are " + countGenes(dna) + " genes in the give DNA strand");
       
   }
}