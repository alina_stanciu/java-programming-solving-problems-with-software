import edu.duke.StorageResource;
import edu.duke.FileResource;

public class Part1 {

   public int findStopCodon(String dnaStr, int startIndex, String stopCodon) {
    
        int currentIndex = dnaStr.indexOf(stopCodon, startIndex + 3);
        
        while(currentIndex != -1){
            int difference = currentIndex - startIndex;
            
            if (difference % 3 == 0) {
                return currentIndex;
            } else {
                currentIndex = dnaStr.indexOf(stopCodon, currentIndex + 1);
            }
        }
        return -1;
   }
    
   public String findGene(String dna, int where) {
    
        int startIndex = dna.indexOf("ATG", where);
        
        if(startIndex == -1) {
            return "";
        }

        int taaIndex = findStopCodon(dna, startIndex, "TAA");
        int tagIndex = findStopCodon(dna, startIndex, "TAG");
        int tgaIndex = findStopCodon(dna, startIndex, "TGA");

        int minIndex = 0;

        if (taaIndex == -1 || (tgaIndex != -1 && tgaIndex < taaIndex)) {
            minIndex = tgaIndex;
        } else {
            minIndex = taaIndex;
        }
        
        if (minIndex == -1 || (tagIndex != -1 && tagIndex < minIndex)) {
            minIndex = tagIndex;
        }
        
        if (minIndex == -1) {
            return "";
        }
        return dna.substring(startIndex, minIndex + 3);
   }
   
   public StorageResource getAllGenes(String dna) {
    
        //Create an empty StorageResource and call it geneList
        StorageResource geneList = new StorageResource();
        //Set startIndex to 0
        int startIndex =0;
        //Repeat the following steps:
        while(true) {
            //Find the next gene after startIndex
            String currentGene = findGene(dna, startIndex);
            //If no gene was found, leave this loop
            if(currentGene.isEmpty()){
                break;
            }
            //Add that gene to geneList
            geneList.add(currentGene);
            //Set startIndex to just go past the end of the gene
            startIndex = dna.indexOf(currentGene, startIndex) + currentGene.length();
        }
        //Your answer is geneList
        return geneList;
   }
   
   public void testOn(String dna) {
   
       System.out.println("Testing getAllGenes on " + dna);
       StorageResource genes = getAllGenes(dna);
       for (String g : genes.data()) {
           System.out.println(g);
       }
   }
   
   public void test() {
   
       testOn("ATGATCTAATTTATGCTGCAACGGTGAAGA");
       
       testOn("ATGATCATAAGAAGATAATAGAGGGCCATGTAA");
   }
   
   public float cgRatio(String dna) {
   
       int firstOccuranceC = dna.indexOf("C");
       int firstOccuranceG = dna.indexOf("G");
       
       int countC = 0;
       int countG = 0;
       
       while(firstOccuranceC != -1 && dna.indexOf("C", firstOccuranceC) != -1) {
           countC++;
           //System.out.println(countC);
           firstOccuranceC = dna.indexOf("C", firstOccuranceC + 1);
       }
       
       while(firstOccuranceG != -1 && dna.indexOf("G", firstOccuranceG) != -1) {
           countG++;
           //System.out.println(countG);
           firstOccuranceG = dna.indexOf("G", firstOccuranceG + 1);
       }
       
       float ratio = (float)countC / countG;
       
       return ratio;
   }
   
   public void testCGratio() {
    
       String dna = "";
       
       dna = "CCGGATGAATT";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The CG ration is: " + cgRatio(dna));
       
       dna = "GCCGTATGTAGTGATAA";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The CG ration is: " + cgRatio(dna));
       
       dna = "ATGTAGCCGGCCGGCGCG";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The CG ration is: " + cgRatio(dna));
   }
   
   public int countCTG(String dna) {
    
       int firstOccuranceCTG = 0;
       
       int countCTG = 0;
       
       while(firstOccuranceCTG != -1 && dna.indexOf("CTG", firstOccuranceCTG + 3) != -1) {
           countCTG++;
           //System.out.println(countC);
           firstOccuranceCTG = dna.indexOf("CTG", firstOccuranceCTG + 3);
       }
       return countCTG;
   }
   
   public void testCountCTG() {
    
       String dna = "";
   
       dna = "CTGCTGCTGCTGCTGCTG";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The codon CTG appears " + countCTG(dna) + " time(s)");
       
       dna = "CCGGATGGGAACCTAGC";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The codon CTG appears " + countCTG(dna) + " time(s)");
       
       dna = "AACCCTGTTCTGGCCTGCTG";
       System.out.println("The DNA strand is: " + dna);
       System.out.println("The codon CTG appears " + countCTG(dna) + " time(s)");
   }
   
   public void processGenes(StorageResource sr) {
    
       int countLongerThan9 = 0;
       int countBiggerRatio = 0;
       int geneSize =0;
       
       System.out.println("The genes with more than 9 characters are:");
       
       for(String genes9 : sr.data()){
           
           if(genes9.length() > 9){
               System.out.println(genes9);
               countLongerThan9++;
            }
        }
       System.out.println("The number of genes with more than 9 characters is: " + countLongerThan9);
   
       for(String cgRat : sr.data()){
           
           if(cgRatio(cgRat) > 0.35){
               countBiggerRatio++;
               System.out.println("Gene with the CG ratio higher than 0.35: " + cgRat + ", the CG ratio being " + cgRatio(cgRat));
           }
       }
       System.out.println("The number of genes with the CG ratio higher than 0.35 is: " + countBiggerRatio);
       
       for(String longest : sr.data()) {
           
           if(longest.length() > geneSize){
               geneSize = longest.length();
           }
       }
       System.out.println("The length of the longest gene is: " + geneSize);
   }
   
   public void testProcessGenes() {
    
       StorageResource sr = new StorageResource();
       
       sr.add("ATGCCGCGGTAG");
       sr.add("CCGGATGAAATAA");
       sr.add("ATGTAACGC");
       sr.add("CGATGATATATATATAACG");
       sr.add("ATGAATTAATTAATTAATTTAG");
       
       System.out.println("This is my list of genes:");
       
       for(String s : sr.data()) {
           System.out.println(s);
        }
       processGenes(sr);
   }
   
   public void testProcessGenesFromFile(){
       FileResource fr = new FileResource("data/brca1.fa");
       String dna = fr.asString().toUpperCase();
       
       System.out.println(countCTG(dna));
        
       System.out.println("The DNA strand is " + dna);
       StorageResource geneList = getAllGenes(dna);
       
       for (String s: geneList.data()){
          System.out.println(s);
       }
       
       processGenes(geneList);
   }
   
   public void processGenes2(StorageResource sr){

        int geneCount = 0;
        int geneCountAbove60 = 0;
        int cgRatioCount = 0;
        int geneLength = 0;
        String longestGene = "";
        
        for (String s: sr.data()){
            geneCount++;
            
              if (s.length() > 60){ 
                //System.out.println("This string has a length greater than 60: " + s);
                geneCountAbove60 = geneCountAbove60 + 1;           
            }
            
            float out = cgRatio(s);
            
            if (out >= 0.35){
               // System.out.println("This string has a C-G-ratio greater than .35: " + s);
                cgRatioCount = cgRatioCount + 1;
            }
            
            if (s.length() > geneLength){
                geneLength = s.length();
                longestGene = s;
            }
        }
        
        System.out.println("The number of strings in sr longer than 60 characters: " + geneCountAbove60);
        System.out.println("The number of strings in sr with C-G-ratio higher than 0.35: " + cgRatioCount);
        System.out.println("Length of longest gene: " + geneLength);
        System.out.println("Longest gene is: " + longestGene);
        System.out.println("Number of genes in the storage list is: " + geneCount);
    }   
}