import edu.duke.*;
import org.apache.commons.csv.*;

public class WhichCountriesExport {

    public void listExporters(CSVParser parser, String exportOfInterest){
        //For each row in the CSV File
        for(CSVRecord record : parser) {
            //Look at the "Exports" column
            String export = record.get("Exports");
            //Check if it contains exportOfInterest
            if(export.contains(exportOfInterest)) {
                //If so, write down the country from that row
                String country = record.get("Country");
                System.out.println(country);
            }
        }
    }
    
    public void whoExportsCoffe(){
    
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        listExporters(parser, "coffee");
    }
    
    public void tester() {
    
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        
        countryInfo(parser, "Germany");
        parser = fr.getCSVParser();
        
        listExportersTwoProducts(parser, "gold", "diamonds");
        parser = fr.getCSVParser();
        
        System.out.println("The number of countries that export gold is: " + numberOfExporters(parser, "gold"));
        parser = fr.getCSVParser();
        
        bigExporters(parser, "$999,999,999,999");
        parser = fr.getCSVParser();
        
        System.out.println("The number of countries that export cocoa is: " + numberOfExporters(parser, "cocoa"));
        parser = fr.getCSVParser();
        
    }
    
    public void countryInfo(CSVParser parser, String country) {
        
        boolean found = false;
        
        for(CSVRecord record : parser) {
            String getCountry = record.get("Country");
            
            if(getCountry.contains(country)) {
                System.out.println(getCountry + ": " + record.get("Exports") + ": " + record.get("Value (dollars)"));
                found = true;
            }
        }  
        
        if(!found) {
            System.out.println("Not found!!");
        }
    }
    
    public void listExportersTwoProducts(CSVParser parser, String exportItem1, String exportItem2) {
    
        for(CSVRecord record : parser) {
            String getExportItem = record.get("Exports");
            
            if(getExportItem.contains(exportItem1) && getExportItem.contains(exportItem2)) {
                System.out.println(record.get("Country"));
            }
        }
    }
    
    public int numberOfExporters(CSVParser parser, String exportItem) {
        
        int countExporters = 0;
        
        for(CSVRecord record : parser) {
            String getExportItem = record.get("Exports");
            
            if(getExportItem.contains(exportItem)) {
                countExporters++;
            }
        }
        return countExporters;
    }
    
    public void bigExporters(CSVParser parser, String amount) {
        
        String referenceAmount = "$999,999,999,999";
        
        for(CSVRecord record : parser) {
            String getValue = record.get("Value (dollars)");
            
            if(getValue.length() > referenceAmount.length()) {
                System.out.println(record.get("Country") + " " + getValue);
            }
        }
    }
}