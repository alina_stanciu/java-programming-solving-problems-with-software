import edu.duke.*;
import org.apache.commons.csv.*;
import java.io.*;

public class WeatherData {

    public CSVRecord coldestHourInFile(CSVParser parser) {
        
        CSVRecord coldestSoFar = null;
        
        for(CSVRecord currentRow : parser) {
            
            if(coldestSoFar == null) {
                coldestSoFar = currentRow;
            } else {
                double currentTemp = Double.parseDouble(currentRow.get("TemperatureF"));
                double coldestTemp = Double.parseDouble(coldestSoFar.get("TemperatureF"));
                
                if(currentTemp < coldestTemp) {
                    coldestSoFar = currentRow;
                }
            }
        }
        return coldestSoFar;
    }
    
    public void testColdestHourInFile() {
        
        FileResource fr = new FileResource("nc_weather/2014/weather-2014-05-01.csv");
        CSVRecord coldest = coldestHourInFile(fr.getCSVParser());
        System.out.println("The coldest temperature was " + coldest.get("TemperatureF") + " at " + coldest.get("TimeEDT"));
    }
    
    public String fileWithColdestTemperature() {
        
        File fileName = null;
        CSVRecord coldestTemp = null;
        DirectoryResource dr = new DirectoryResource();
        
        for(File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            CSVParser parser = fr.getCSVParser();
            CSVRecord currentRow = coldestHourInFile(parser);
            
            if(coldestTemp == null) {
                coldestTemp = currentRow;
                fileName = f;
            } else {
                double currentTemp = Double.parseDouble(currentRow.get("TemperatureF"));
                double lowestTemp = Double.parseDouble(coldestTemp.get("TemperatureF"));
                
                if(currentTemp < lowestTemp && currentTemp != -9999.0) {
                    coldestTemp = currentRow;
                    fileName = f;
                }
            }
        }
        return fileName.getAbsolutePath();
    }
    
    public void testFileWithColdestTemperature() { 
        
        String fileWithColdestTemperature = fileWithColdestTemperature();
        File f = new File(fileWithColdestTemperature);
        String fileName = f.getName();
        System.out.println("The coldest day is in file: " + fileName);
        
        FileResource fr = new FileResource(f);
        CSVParser parser = fr.getCSVParser();
        CSVRecord lowestTemp = coldestHourInFile(parser);
        System.out.println("The coldest temperature is: " + lowestTemp.get("TemperatureF"));
        
        System.out.println("All the temperatures on the coldest day are:");
        CSVParser parser2 = fr.getCSVParser();
        
        for(CSVRecord record : parser2) {
            double temperature = Double.parseDouble(record.get("TemperatureF"));
            System.out.println(record.get("DateUTC") + " " + record.get("TimeEST") + " " + temperature);
        }
    }
    
    public CSVRecord lowestHumidityInFile(CSVParser parser) {
        
        CSVRecord lowestHumidity = null;
        
        int currentHumidity = 0;
        int lowestHumd = 0;
        
        for(CSVRecord currentRow : parser) {
            
            if(lowestHumidity == null) {
                lowestHumidity = currentRow;
            } else {
                if(!currentRow.get("Humidity").equals("N/A") && !lowestHumidity.get("Humidity").equals("N/A")) {
                    currentHumidity = Integer.parseInt(currentRow.get("Humidity"));
                    lowestHumd = Integer.parseInt(lowestHumidity.get("Humidity"));
                    
                    if(currentHumidity < lowestHumd) {
                        lowestHumidity = currentRow;
                    }
                }
            }
        }
        return lowestHumidity; 
    }
    
    public void testLowestHumidityInFile() {
        
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        CSVRecord lowestHumidity = lowestHumidityInFile(parser);
        
        System.out.println("Lowest humidity is " + lowestHumidity.get("Humidity") + " at " + lowestHumidity.get("DateUTC"));
    }
    
    public CSVRecord lowestHumidityInManyFiles() {
        
        CSVRecord lowestHumidity = null;
        
        int currentHumidity = 0;
        int lowestHumd = 0;
        
        DirectoryResource dr = new DirectoryResource();
        
        for(File f : dr.selectedFiles()) {
            FileResource fr = new FileResource(f);
            CSVParser parser = fr.getCSVParser();
            CSVRecord currentRow = lowestHumidityInFile(parser);
            
            if(lowestHumidity == null) {
                lowestHumidity = currentRow;
            } else {
                int currentTemp = Integer.parseInt(currentRow.get("Humidity"));
                int lowestTemp = Integer.parseInt(lowestHumidity.get("Humidity"));
                
                if(currentTemp < lowestTemp) {
                    lowestHumidity = currentRow;
                } else {
                    
                    if (currentRow.get("Humidity") != "N/A" && lowestHumidity.get("Humidity") != "N/A") {
                        currentHumidity = Integer.parseInt(currentRow.get("Humidity"));
                        lowestHumd = Integer.parseInt(lowestHumidity.get("Humidity"));
                        
                        if(currentHumidity < lowestHumd) {
                            lowestHumidity = currentRow;
                        }
                    }
                }
            }
        }
        return lowestHumidity;    
    }
    
    public void testLowestHumidityInManyFiles() {
        
        CSVRecord lowestHumidity = lowestHumidityInManyFiles();
        
        System.out.println("The lowest humidity was " + lowestHumidity.get("Humidity") + " at " + lowestHumidity.get("DateUTC"));

    }
    
    public double averageTemperatureInFile(CSVParser parser) {
        
        double sumOfTemperatures = 0.0;
        double countTemperatures = 0.0;
        
        for(CSVRecord record : parser) {
            double temperature = Double.parseDouble(record.get("TemperatureF"));
            sumOfTemperatures += temperature;
            countTemperatures++;
        }
        
        double average = sumOfTemperatures / countTemperatures;
        
        return average;
    }
    
    public void testAverageTemperatureInFile() {
        
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        double average = averageTemperatureInFile(parser);
        
        System.out.println("Average temperature in file is " + average);
    }
    
    public double averageTemperatureWithHighHumidityInFile(CSVParser parser, int value) {
        
        double sumOfTemperatures = 0.0;
        double countTemperatures = 0.0;
        
        for(CSVRecord record : parser) {
            double temperature = Double.parseDouble(record.get("TemperatureF"));
            int humidity = Integer.parseInt(record.get("Humidity"));
            
            if(humidity >= value) {
                sumOfTemperatures += temperature;
                countTemperatures++;
            }
        }
        double average = sumOfTemperatures / countTemperatures;
        
        return average;
    }
    
    public void testAverageTemperatureWithHighHumidityInFile() {
        
        FileResource fr = new FileResource();
        CSVParser parser = fr.getCSVParser();
        double average = averageTemperatureWithHighHumidityInFile(parser, 80);
        
        if(Double.isNaN(average)){
            System.out.println("No temperature was found!");
        } else {
            System.out.println("The average temperature is " + average);
        }
        }
}