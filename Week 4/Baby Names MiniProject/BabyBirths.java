import edu.duke.*;
import org.apache.commons.csv.*;
import java.io.*;

public class BabyBirths {
    
    public void printNames () {
        
        FileResource fr = new FileResource();
        
        for (CSVRecord record : fr.getCSVParser(false)) {
            int numBorn = Integer.parseInt(record.get(2));
            
            if (numBorn <= 100) {
                System.out.println("Name " + record.get(0) +
                           " Gender " + record.get(1) +
                           " Num Born " + record.get(2));
            }
        }
    }

    public void totalBirths (FileResource fr) {
        
        int totalBirths = 0;
        int totalBoys = 0;
        int totalGirls = 0;
        int totalUniqueBoys = 0;
        int totalUniqueGirls = 0;
        
        for (CSVRecord record : fr.getCSVParser(false)) {
            int numberBorn = Integer.parseInt(record.get(2));
            totalBirths += numberBorn;
            
            if (record.get(1).equals("M")) {
                totalBoys += numberBorn;
                totalUniqueBoys++;
            } else {
                totalGirls += numberBorn;
                totalUniqueGirls++;
            }
        }
        System.out.println("Total births: " + totalBirths);
        System.out.println("Total boys born: " + totalBoys);
        System.out.println("Total girls born: " + totalGirls);
        System.out.println("Total boys names: " + totalUniqueBoys);
        System.out.println("Total girls names: " + totalUniqueGirls);
        System.out.println("Total unique names: " + (totalUniqueBoys + totalUniqueGirls));
    }

    public void testTotalBirths () {
        
        //FileResource fr = new FileResource();
        FileResource fr = new FileResource("data/yob1905.csv");
        totalBirths(fr);
    }
    
    public int getRank(int year, String name, String gender) {
        
        int rank = 1;
        
        FileResource fr = new FileResource("data/yob" + year + ".csv");
        CSVParser parser = fr.getCSVParser(false);
        
        for(CSVRecord record : parser) {
            String currentName = record.get(0);
            String currentGender = record.get(1);
            
            if(currentGender.equals(gender)) {
                
               if(currentName.equals(name)) {
                return rank;
               }
               rank++;
            }
        }
        return -1;
    }
    
    public void testGetRank() {
        
        int year = 1971;
        String name = "Frank";
        String gender = "M";
        
        int rank = getRank(year, name, gender);
        System.out.println("The rank of " + name + ", " + gender + ", in " + year + " is: " + rank);
    }
    
    public String getName(int year, int rank, String gender) {
        
        int currentRank = 0;
        String name = " ";
        
        FileResource fr = new FileResource("data/yob" + year + ".csv");
        CSVParser parser = fr.getCSVParser(false);
        
        for(CSVRecord record : parser) {
            String currentName = record.get(0);
            String currentGender = record.get(1);
            
            if(currentGender.equals(gender)) {
                
                if(currentRank == rank) {
                    return name;
                }
                name = currentName;
                currentRank++;
            }
        }
        return "NO NAME";
    }
    
    public void testGetName() {
        
        int year = 1982;
        int rank = 450;
        String gender = "M";
        
        String name = getName(year, rank, gender);
        System.out.println("In " + year + ", the " + gender + " at rank " + rank + " was " + name);
    }
    
    public void whatIsNameInYear(String name, int year, int newYear, String gender) {
        
        int rank = getRank(year, name, gender);
        
        System.out.println("The rank of " + name + " is: " + rank);
        
        String newName = getName(newYear, rank, gender);
        
        System.out.println(name + " born in " + year + " would be " + newName + " if born in " + newYear);
    }
    
    public void testWhatIsNameInYear() {
        
        whatIsNameInYear("Owen", 1974, 2014, "M");
    }
    
    public int yearOfHighestRank(String name, String gender) {
        
        DirectoryResource dr = new DirectoryResource();
        
        int year = 0;
        int rank = 0;
        
        for(File f : dr.selectedFiles()) {
            int currentYear = Integer.parseInt(f.getName().substring(3,7));
            int currentRank = getRank(currentYear, name, gender);
            
            System.out.println("The rank in year " + currentYear + " is: " + currentRank);
            
            if(currentRank != -1) {
                
                if(rank == 0 || currentRank < rank) {
                    rank = currentRank;
                    year = currentYear;
                }
            }
        }
        return year;        
    }
    
    public void testYearOfHighestRank() {
        
        String name = "Genevieve";
        String gender = "F";
        
        System.out.println("The year with the highest rank for " + name + ", " + gender + " is " + yearOfHighestRank(name, gender));
    }
    
    public double getAverageRank(String name, String gender) {
        
        DirectoryResource dr = new DirectoryResource();
        
        double totalRank = 0.0;
        int countRanks = 0;
        
        for(File f : dr.selectedFiles()) {
            int currentYear = Integer.parseInt(f.getName().substring(3,7));
            int currentRank = getRank(currentYear, name, gender);
            
            totalRank += currentRank;
            countRanks++;
        }
        
        if(totalRank == 0.0) {
            return -1.0;
        }
        
        double average = totalRank / countRanks;
        
        return average;
    }
    
    public void testGetAverageRank() {
        
        String name = "Robert";
        String gender = "M";
        double average = getAverageRank(name, gender);
        
        System.out.println("The average rank for " + name + ", " + gender + " is: " + average);
    }
    
    public int getTotalBirthsRankedHigher(int year, String name, String gender) {
        
        FileResource fr = new FileResource("data/yob" + year + ".csv");
        CSVParser parser = fr.getCSVParser(false);
        
        boolean found = false;
        int sum = 0;
        
        for(CSVRecord record : parser) {
            String currentName = record.get(0);
            String currentGender = record.get(1);
            
            if(currentGender.equals(gender)) {
                if(currentName.equals(name)) {
                    found = true;
                    break;
                }
                sum += Integer.parseInt(record.get(2));
            }
        }
        
        if(found) {
            return sum;
        } else {
            return -1;
        }
    }
    
    public void testGetTotalBirthsRankedHigher() {
        
        int year = 1990;
        String name = "Emily";
        String gender = "F";
        
        int sum = getTotalBirthsRankedHigher(year, name, gender);
        
        System.out.println("The total births ranked higher than " + name + ", " + gender + " in " + year + " is: " + sum);
    }
}