import edu.duke.*;
import java.io.*;

public class GrayscaleConverter {

    public ImageResource makeGray(ImageResource inImage) {
        
        ImageResource outImage = new ImageResource(inImage.getWidth(), inImage.getHeight());
        
        for(Pixel pixel : outImage.pixels()) {
            Pixel inPixel = inImage.getPixel(pixel.getX(), pixel.getY());
            
            int average = (inPixel.getRed() + inPixel.getBlue() + inPixel.getGreen()) / 3;
            
            pixel.setRed(average);
            pixel.setGreen(average);
            pixel.setBlue(average);
        }
        return outImage;
    }
    
    public void selectAndConvert() {
        
        DirectoryResource dr = new DirectoryResource();
        
        for(File file : dr.selectedFiles()) {
            ImageResource originalImage = new ImageResource(file);
            ImageResource grayImage = makeGray(originalImage);
            
            String fname = originalImage.getFileName();
            String newName = "gray-" + fname;
            
            grayImage.setFileName(newName);
            
            grayImage.draw();
            grayImage.save();
        }
        
    }
}