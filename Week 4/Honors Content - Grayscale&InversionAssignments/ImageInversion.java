import edu.duke.*;
import java.io.*;

public class ImageInversion {

    public ImageResource makeInversion(ImageResource inImage) {
        
        ImageResource outImage = new ImageResource(inImage.getWidth(), inImage.getHeight());
        
        for(Pixel pixel : outImage.pixels()) {
            Pixel inPixel = inImage.getPixel(pixel.getX(), pixel.getY());
            
            int red = 255 - inPixel.getRed();
            int blue = 255 - inPixel.getBlue();
            int green = 255 - inPixel.getGreen();
            
            pixel.setRed(red);
            pixel.setGreen(green);
            pixel.setBlue(blue);
        }
        return outImage;
    }
    
    public void selectAndConvert() {
        
        DirectoryResource dr = new DirectoryResource();
        
        for(File file : dr.selectedFiles()) {
            ImageResource originalImage = new ImageResource(file);
            ImageResource invertedImage = makeInversion(originalImage);
            
            String fname = originalImage.getFileName();
            String newName = "inverted-" + fname;
            
            invertedImage.setFileName(newName);
            
            invertedImage.draw();
            invertedImage.save();
        }
    }
}